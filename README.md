Wireworld for emacs!

![Wireworld running in emacs](http://dustycloud.org/misc/emacs-wireworld.png)

Yes, now you can run
[Wireworld](https://en.wikipedia.org/wiki/Wireworld) in emacs!
Ever wanted to turn your ascii art into a
[running computer](https://www.quinapalus.com/wi-index.html)?
Now you can!

A little demo in the wireworld-demo.ww file.  Load up wireworld-mode,
then simply type ```C-c C-c``` to advance the buffer.
