;; -*- lexical-binding: t -*-

;;; wireworld-el --- Run Wireworld cellular automata
;;;
;;; Copyright (C) 2017  Christine Lemmer-Webber <cwebber@dustycloud.org>
;;;
;;; This file is not part of GNU Emacs.
;;;
;;; This file is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3, or (at your option)
;;; any later version.
;;;
;;; This file is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Wireworld is a nifty type of "celluar automata".  You can draw out
;; little circuits and then simulate them.
;;
;; In wireworld-el's case, there are three key characters, which you can
;; draw freely in the buffer in plain old ascii art:
;;
;;    . => wire
;;    @ => electron "head"
;;    * => electron "tail"
;;      => (whitespace) empty
;;
;; Any characters which are not the above are ignored, which means you are
;; free to type whatever comments you want... as long as they don't
;; involve ., @, or * :)

;;; Code:


(defun wireworld-empty-2d-vector (width height)
  (let ((vec (make-vector height nil)))
    (dotimes (i height)
      (aset vec i (make-vector width nil)))
    vec))

(defun wireworld-buffer-to-2d-vector ()
  "Transform current buffer into a wireworld buffer"
  (save-excursion
    (let* ((height (line-number-at-pos (point-max)))
           (width (save-excursion
                    (goto-char (point-min))
                    (let ((biggest 0))
                      (while (not (= (point-max) (point)))
                        (let ((line-width (- (line-end-position)
                                             (line-beginning-position))))
                          (if (> line-width biggest)
                              (setq biggest line-width)))
                        (forward-line))
                      biggest)))
           (ww-vector (wireworld-empty-2d-vector width height)))
      ;; fill ww-vector
      (goto-char (point-min))
      (let ((cur-row 0)
            (cur-col 0)
            (pmax (point-max)))
        (while (< (point) pmax)
          (let ((char (char-after (point))))
            (if (eq char ?\n)
                ;; If it's a newline...
                (progn
                  ;; advance row and reset column
                  (setq cur-row (+ cur-row 1))
                  (setq cur-col 0))
              (progn
                (aset (aref ww-vector cur-row) cur-col
                      char)
                (setq cur-col (+ cur-col 1))))
            (forward-char))))
      ww-vector)))

;; @@: Not sure if needed or not...
(defun wireworld-copy-2d-vector (2d-vector)
  (let* ((height (length 2d-vector))
         (width (length (aref 2d-vector 0)))
         (new-vector (wireworld-empty-2d-vector width height)))
    (dotimes (h height)
      (let ((old-row (aref 2d-vector h))
            (new-row (aref new-vector h)))
        (dotimes (w width)
          (aset new-row w (aref old-row w)))))
    new-vector))

(defconst wireworld-celltypes
  '((empty . ?\s)
    (head . ?@)
    (tail . ?*)
    (conductor . ?.))
  "Cell types for wireworld.  An alist of symbol to printed representation.")

(defun wireworld-celltype-char (celltype)
  (cdr (assq celltype wireworld-celltypes)))

(defconst wireworld-celltype-head
  (wireworld-celltype-char 'head))

(defun wireworld-1-if-head-at (2d-vector w h)
  (let ((height (length 2d-vector))
        (width (length (aref 2d-vector 0))))
    (cond ((or (< w 0) (< h 0)
               (>= w width) (>= h height))
           0)
          ((eq (aref (aref 2d-vector h) w) wireworld-celltype-head)
           1)
          (t 0))))

(defun wireworld-heads-neighboring (2d-vector w h)
  (+ (wireworld-1-if-head-at 2d-vector (- w 1) (- h 1))   ; nw
     (wireworld-1-if-head-at 2d-vector w       (- h 1))   ; n
     (wireworld-1-if-head-at 2d-vector (+ w 1) (- h 1))   ; ne
     (wireworld-1-if-head-at 2d-vector (+ w 1) h      )   ; e
     (wireworld-1-if-head-at 2d-vector (+ w 1) (+ h 1))   ; se
     (wireworld-1-if-head-at 2d-vector w       (+ h 1))   ; s
     (wireworld-1-if-head-at 2d-vector (- w 1) (+ h 1))   ; sw
     (wireworld-1-if-head-at 2d-vector (- w 1) h      ))) ; w

(defun wireworld-advance (2d-vector)
  (let* ((height (length 2d-vector))
         (width (length (aref 2d-vector 0)))
         (new-vector (wireworld-empty-2d-vector width height)))
    (dotimes (h height)
      (let ((old-row (aref 2d-vector h))
            (new-row (aref new-vector h)))
        (dotimes (w width)
          (let* ((old-char (aref old-row w))
                 (celltype-entry (rassq old-char wireworld-celltypes))
                 (new-char
                  (if celltype-entry
                      (let ((celltype (car celltype-entry)))
                        (wireworld-celltype-char
                         (cond
                          ;; Empty -> Empty 
                          ((eq celltype 'empty)
                           'empty)
                          ;; Head -> Tail
                          ((eq celltype 'head)
                           'tail)
                          ;; Tail -> Conductor
                          ((eq celltype 'tail)
                           'conductor)
                          ;; Conductor ->
                          ;;   Head {if exactly 1 or 2 neighbors are heads}
                          ;;   Conductor {otherwise}
                          ((eq celltype 'conductor)
                           (let ((head-neighbors (wireworld-heads-neighboring
                                                  2d-vector w h)))
                             (if (or (= head-neighbors 1)
                                     (= head-neighbors 2))
                                 'head
                               'conductor))))))
                    old-char)))
            (aset new-row w new-char)))
        (aset new-vector h new-row)))
    new-vector))

;; @@: Maybe we should "write over" the characters that have changed instead
(defun wireworld-replace-buffer-with (2d-vector)
  "Replace buffer with contents of 2d-vector"
  (let ((orig-pos (point))
        (height (length 2d-vector))
        (width (length (aref 2d-vector 0))))
    (erase-buffer)
    (dotimes (h height)
      (dotimes (w width)
        (let ((char (aref (aref 2d-vector h) w)))
          (when char
            (insert-char char))))      
      (when (not (= h (- height 1)))
        (insert ?\n)))
    ;; Reset cursor, assuming still within buffer size
    (when (not (> orig-pos (point-max)))
      (goto-char orig-pos))))

(defun wireworld-advance-buffer ()
  "Advance the buffer... by replacing its contents!"
  (interactive)
  (wireworld-replace-buffer-with (wireworld-advance (wireworld-buffer-to-2d-vector))))


;;; Major mode stuff

(defvar wireworld-map (make-sparse-keymap)
  "Keymap for wireworld-mode")

(define-key wireworld-map "\C-c\C-c"
  'wireworld-advance-buffer)

(defgroup wireworld-faces nil
  "Wireworld mode specific faces"
  :group 'font-lock)

(defface wireworld-wire-face
  '((((class color) (background dark))
     (:foreground "light steel blue" :bold t)))
  "Wireworld face for wires"
  :group 'wireworld-faces)

(defvar wireworld-wire-face
  'wireworld-wire-face
  "Face name to use for wireworld wires")

(defface wireworld-electron-head-face
  '((((class color) (background dark))
     (:foreground "gold1" :bold t)))
  "Wireworld face for electron heads"
  :group 'wireworld-faces)

(defvar wireworld-electron-head-face
  'wireworld-electron-head-face
  "Face name to use for wireworld electron heads")

(defface wireworld-electron-tail-face
  '((((class color) (background dark))
     (:foreground "DarkOrange3" :bold t)))
  "Wireworld face for electron tails"
  :group 'wireworld-faces)

(defvar wireworld-electron-tail-face
  'wireworld-electron-tail-face
  "Face name to use for wireworld electron tails")

(define-derived-mode wireworld-mode text-mode
  "Wireworld"
  (toggle-truncate-lines t)
  (setq-local font-lock-defaults
              '((("@" . wireworld-electron-head-face)
                 ("\\*" . wireworld-electron-tail-face)
                 ("\\." . wireworld-wire-face)
                 ("[^@\\*\\.]" . font-lock-comment-face))))
  (use-local-map wireworld-map))

(defvar wireworld-default-sleeptime
  .2
  "Default amount of time to sleep during a simulation")

(define-derived-mode wireworld-simulation-mode wireworld-mode
  "Wireworld mode for running a long-term simulation.  Won't keep
undo history, etc."
  (buffer-disable-undo))

(defun wireworld-simulate-buffer (&optional sleeptime)
  "Copy current buffer to a *simulation* buffer"
  (interactive "p")
  (or sleeptime (setq sleeptime wireworld-default-sleeptime))
  (let ((orig-string (buffer-string)))
    (switch-to-buffer (get-buffer-create "*Wireworld*") t)
    (erase-buffer)
    (insert orig-string)
    (wireworld-simulation-mode)

    ;; borrowed from life.el
    (catch 'wireworld-exit
      (while t
        (wireworld-advance-buffer)
        (or (and (sit-for sleeptime) (< 0 sleeptime))
            (not (input-pending-p))
            (throw 'wireworld-exit nil))))))


(provide 'wireworld)
